This is the parent project for all the custom maven plugins

This project is open-source, based on the Apache License version 2.0.

At present the following plugins have been developed -
	1. scriptnumber-buildbreaker - Plugin to manage the DB script numbers across various branches.